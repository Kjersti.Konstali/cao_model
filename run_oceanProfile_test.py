#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 8 2024

@author: kko033
"""


#%%
from modcor.main import modCor

#%%
# INstead of using these classes, you can just import the initial values directly
#and pass them onto the modCor function... not sure about the best practice here.   
class reference(object):
    name = 'ocean_prof_test'
    def __init__(self, name):
        self.name = name
    import initdata.modinit_ocean_prof_test as mi
    
#class test(object): 
#    name = 'ocean_prof_test_NABOS_SI_T8_444'
#    def __init__(self, name):
#        self.name = name
#    import initdata.modinit_ocean_prof_test as mi
    
#%%

for t in [reference]:

    print(t.name)
  
    ds = modCor(t.mi)
    comp = dict(zlib=True, complevel=5)
    encoding = {var: comp for var in ds.data_vars}    

    ds.to_netcdf('./runs/{}.nc'.format(t.name), encoding=encoding)


# %%
