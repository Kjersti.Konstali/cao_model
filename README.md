
# Running the model
To run the model, you run: `run_model.py`. This script feeds the model with the initial data, stored in `initdata`-folder. When doing a large sensitivity study, it is possible to alter the initial values without creating a new initial values-file. Example on how to vary the sea ice conditions, as well as the ocean coupling, is in `run_SI_cases.py`. 

The core of the model is in `main.py`, but it shouldn't be necessary to interact with this script directly. 
