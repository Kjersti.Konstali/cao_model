#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  9 08:50:03 2024

@author: kjersti
"""

import xarray as xr
import proplot as pplt
import numpy as np 
import initdata.init_fun as init
import seawater as sw


#%%
xmax      = 750            # How far you want to integrate (in km)
deltax    = 0.1             #Resolution (in km) (doesn't work well if you make it coarser)
x         = np.arange(deltax,xmax+deltax,deltax)*1e3 

hepsilon = 0.002  # Convergence criterion 
N = 3000          # max  ABL height you expect through the iteration (how large you initialize arrays)


moisture  = True
ocean     = True
seaice    = True
tau       = 8*24*60*60      #"Length" of CAO outbreak on the ocean in seconds


# ABL parametrs
thetam0   = 253             # ABL mixed layer temperature
gammath0 = 0.012            # Background stability
um0     = 10                # mixed layer wind speed
h0      = 100               # Initial ABL height
qv0     = 0.30e-3           # Intial moisture content
qc0     = 0                 #No liquid water to start out with
upsilon = 0.2               #Entrainment parameter
qb0     = 0                 #background moistre profile; not IMPLEMENTED
pm0 = 1000

# Ice quantities
ice_init  = np.zeros(len(x))#init.eaMIZ_fun(int(90/deltax), x, 'tanhL')#
OW    = 1-ice_init#np.ones(len(x))
SI      = 'none'


#Ocean mixed layer quantities
depth_m = 1500          # Depth of mixed layer profiles 


#%%
S = xr.open_dataset('./initdata/CTD/s13_01.nc', decode_times=False).s_an.rename({'depth':'z'}).sel(z=slice(0,depth_m))
T = xr.open_dataset('./initdata/CTD/t13_01.nc', decode_times =False).t_an.rename({'depth':'z'}).sel(z=slice(0,depth_m))

line_points = np.array([[75, -10], [70, 5]])
num_points = 100  # Number of points along the line
lat_line = np.linspace(line_points[0, 0], line_points[1, 0], num_points)
lon_line = np.linspace(line_points[0, 1], line_points[1, 1], num_points)



#fig,ax = pplt.subplots(projection='lcc', proj_kw = dict(lon_0=0), nrows = 2)
#ax.format(coast=True, latlim = [60,85], lonlim = [-50,50])
#ax[0].contourf(S.isel(z=0, time = 0), colorbar = 'b', levels = pplt.arange(30,36,0.25), cmap = 'haline')
#ax[1].contourf(T.isel(z=0, time = 0), colorbar = 'b', levels = pplt.arange(-2,15), cmap = 'BuRd')
#ax.plot(lon_line, lat_line)


#%%
line_data_S = init.make_section(S, lat_line, lon_line)
line_data_T = init.make_section(T, lat_line, lon_line)
distances   = init.calc_distance(lat_line, lon_line)

#%%
# Create a new Dataset for the line section
ocean2D = xr.Dataset(
    {'S': (['z', 'distance'], line_data_S),
     'T': (['z', 'distance'], line_data_T)},
    coords={'z': S['z'].values, 'distance': distances}
).interp(distance=x/1e3, z = pplt.arange(0,depth_m)).interpolate_na('z', fill_value = 'extrapolate').interpolate_na('distance', fill_value = 'extrapolate').transpose('distance','z')

#%%

mld0 = 30

Tprofile = ocean2D.T
Sprofile = ocean2D.S

for i in range(len(x)): 
    Tprofile[i,0:mld0] = np.nanmean(Tprofile[i,0:mld0])#Tm0[0]
    Sprofile[i,0:mld0] = np.nanmean(Sprofile[i,0:mld0])#Sm0[0]

#%%
density= sw.dens0(Sprofile, Tprofile)
ocean2D['sigma'] = (('distance','z'), density-1000)
mld = np.ones(len(x))

#%%
for i, ix in enumerate(x/1e3): 
    ocX = ocean2D.sel(distance=ix)
    mld[i] = (np.where(ocX.sigma > (ocX.sigma.isel(z=0)+0.0125))[0][0])


Sprofile = Sprofile.values
Tprofile = (Tprofile + 273.15).values
Tm0 = Tprofile[:,0]
Sm0 = Sprofile[:,0]

