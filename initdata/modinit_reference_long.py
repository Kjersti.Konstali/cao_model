import numpy as np
import init_fun as init

#Domain setup: 
# -----------------------------------------------------------------------------
xmax      = 1000
deltax    = 0.1             #Resolution (in km)
x         = np.arange(deltax,xmax+deltax,deltax)*1e3

hepsilon = 0.002 # Convergence criterion 
N        = 3000  # max  ABL height you expect through the iteration (how large you initialize arrays) 

# Vary complexities: 
#------------------------------------------------------------------------------
moisture  = True
ocean     = True
seaice_thermo   = False # A priori calculation of sea ice considering just thermodynamics (all is immediately advected away)
seaice    = True # This calculates the sea ice properly
tau       = 2*24*60*60 #"Length" of CAO outbreak on the ocean in seconds
upsilon   = 0.2               #Entrainment parameter


# ABL parametrs
# -----------------------------------------------------------------------------
thetam0   = 253
gammath0  = 0.012
um0       = 10
h0      = 100
qv0     = 0.30e-3
qc0     = 0                 #No liquid water to start out with
qb0     = 0                 #background moisture profile; not sure if it actually works in this script. But if qb = 0; then there is no different from the original script. 
pm0       = 1000            #reference pressure (mb) set to 1000 for simplicity !TODO: Check!  


# Ice quantities
# -----------------------------------------------------------------------------
ice_init = np.zeros(len(x))#init.eaMIZ_fun(int(90/deltax), x, 'step')#
OW       = 1-ice_init
mld0     = 50           #Initial mixed layer depth
N        = 3000
depth_m  = 500
SI       = 'none'

# Ocean profile initialization
# -----------------------------------------------------------------------------
Tprofile = np.ones((len(x), depth_m))
Sprofile = Tprofile.copy()

Tprofile0, Sprofile0 = init.init_CTD(75.5, -7.5, mld0, ideal = True) #Here: 75.5 and -7.5 is latitudes and longitudes of the profile location
Tprofile[:,:] = Tprofile0[0:depth_m]
Sprofile[:,:] = Sprofile0[0:depth_m]

for i in range(len(x)): 
    Tprofile[i,0:mld0] = np.nanmean(Tprofile[i,0:mld0])#Tm0[0]
    Sprofile[i,0:mld0] = np.nanmean(Sprofile[i,0:mld0])#Sm0[0]

Tprofile0 = Tprofile.copy()
Sprofile0 = Sprofile.copy()

Tm0 = Tprofile[:,0]
Sm0 = Sprofile[:,0]

Tm = Tm0.copy()
Sm = Sm0.copy()

# -----------------------------------------------------------------------------

# End of Part where you should change stuff! 

# -----------------------------------------------------------------------------
