import xarray as xr
import numpy as np
import seawater as sw
import scipy.ndimage.filters as filters
from scipy.interpolate import griddata

def init_CTD(lat,lon, mld, ideal = True, x = False): 
    t_data = xr.open_dataset('./initdata/CTD/t13_01.nc', decode_times=False).sel(lat=lat, lon=lon, method='nearest')
    s_data = xr.open_dataset('./initdata/CTD/s13_01.nc', decode_times=False).sel(lat=lat, lon=lon, method='nearest')

    t = t_data.t_an.values.squeeze()
    s = s_data.s_an.values.squeeze()
    #d = t_data.dens0.values.squeeze() 

    z = t_data.depth.values
    zz = np.arange(0,t_data.depth.dropna(dim='depth').values.max())

    sal_interp  = np.interp(zz,z,s)[0:2000]
    temp_interp = np.interp(zz,z,t)[0:2000] + 273.15 #Data is in C
    zz  = zz[0:2000]

    if ideal: 
        temp_interp,sal_interp = CTD_ideal(temp_interp, sal_interp,zz, mld)
    if len(x)>1: 
        temp_interp = np.repeat(temp_interp[np.newaxis,:], len(x), axis = 0)
        sal_interp  = np.repeat(sal_interp[np.newaxis,:], len(x), axis = 0)        
    

    
    return temp_interp, sal_interp


def CTD_cross_test(x): 
    ds = xr.open_dataset('./initdata/CTD/NABOS_2015_for_Kjersti.nc') #This is not necessarily available 
    test = ds.interp(LATITUDE=np.linspace(ds.LATITUDE[0], ds.LATITUDE[-1],len(x))).sel(DEPTH=slice(0,501)).interpolate_na(dim='LATITUDE').sortby('LATITUDE', ascending=False)
    
    T= test.TEMP.values + 273.15
    S= test.SAL.values

    return T.T,S.T

def CTD_ideal(temp_interp, sal_interp, zz, mld): 
                
    s_smooth = filters.gaussian_filter1d(sal_interp,sigma  =  5)
    t_smooth = filters.gaussian_filter1d(temp_interp,sigma = 5)

    ref_d  = 1000-mld

    s_top =  np.ones(mld)*np.mean(s_smooth[0:mld])
    t_top =  np.ones(mld)*np.mean(t_smooth[0:mld])
            
    sprof = np.concatenate((s_top,s_smooth[mld:]),axis=0)
    tprof = np.concatenate((t_top,t_smooth[mld:]),axis=0)

    t_tan = tprof[mld-1] + (tprof[ref_d]-tprof[mld-1])*np.arctan(zz[mld-1:]/75)
    s_tan = sprof[mld-1] + (sprof[ref_d]-sprof[mld-1])*np.arctan(zz[mld-1:]/75)
    
    salpp = s_tan - (s_tan[ref_d]-sprof[ref_d])
    tempp = t_tan - (t_tan[ref_d]-tprof[ref_d])
    
    s_top = np.ones(mld)*salpp[0]
    t_top = np.ones(mld)*tempp[0]
    t_ideal = np.concatenate((t_top,tempp),axis=0)
    s_ideal = np.concatenate((s_top,salpp),axis=0)

    return t_ideal,s_ideal

def eaMIZ_fun(L,x,distr):
    '''
    Initialize MIZ zones with equal areas 

    Parameters
    ----------
    L : 
        length of miz zone.
    x : 
        x from model
    distr : string
        What kind of distribution do you want? 
        Options are lin, tanh, tanhL, tanhU, and step.

    Returns
    -------
    ice : array
        sea ice concentration.
    A : float
        area of sea ice.

    '''
    #The MIZs have the same area of ice (at least almost)

    L_miz = L/3
    y_c = L_miz*1.5
    x = x/1e2
    
    #area of the ice you want 
    A1 = np.trapz(np.linspace(1,0,L))
    
    
    if distr == 'linear' or distr == 'lin' or distr == 'n':
        ice = np.linspace(1,0,L)
        A = np.trapz(ice)
        if len(ice)<len(x):
            ice = np.concatenate((ice,np.zeros(len(x)-len(ice))),axis=0)


    elif distr == 'tanh' or distr == 'h':
        
        for xx in np.linspace(1,8,999):
                ice    = 0.5 - 0.5*np.tanh((x-y_c)/(L_miz/xx))
                A = np.trapz(ice)

                if A<A1:
                    break
        print(xx)

    elif distr == 'tanh lower' or distr == 'tanhL' or distr == 'l' or distr == 'L':

        for xx in np.linspace(1,8,9999):
            A   = np.trapz(2*(0.5 - 0.5*np.tanh(x/(L_miz*xx))))
            if A>A1:
                    break
            ice = 2*(0.5 - 0.5*np.tanh(x/(L_miz*xx)))

    elif distr == 'tanh upper' or distr == 'tanhU' or distr == 'u' or  distr == 'U':
        yy = np.arange(-L*1.5,0)

        for yc in np.linspace(yy[0],y_c/2,9999):
            ice    =  -np.tanh((yy-yc)/(L_miz))
            ice    = ice[ice>0]
            A = np.trapz(ice)
            if A>A1:
                break

        ice  = np.concatenate((ice,np.zeros(len(x)-len(ice))),axis=0)

            
    elif distr == 'step' or distr == 'p':
        for y in np.arange(L_miz/5,L_miz*100):
            ice = np.ones(int(y))
            A = np.trapz(ice)
            if A>A1:
                break
        ice = np.concatenate((ice,np.zeros(len(x)-len(ice))),axis=0)
                     
    else:
        print('you can choose "tanh", "tanh upper", "tanh lower" or "linear"')
        ice = np.nan
    return ice

def haversine(lat1, lon1, lat2, lon2):
    '''
    calculate distance between points in km
    '''
    R = 6371.0  # Earth radius in kilometers

    phi1 = np.radians(lat1)
    phi2 = np.radians(lat2)
    delta_phi = np.radians(lat2 - lat1)
    delta_lambda = np.radians(lon2 - lon1)

    a = np.sin(delta_phi / 2.0) ** 2 + np.cos(phi1) * np.cos(phi2) * np.sin(delta_lambda / 2.0) ** 2
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))

    distance = R * c
    return distance


def make_section(ds, lat_line, lon_line): 
    '''
    Interpolate data onto a line to make a section, note that this loops 
    through the z coordinates of ds. Returns the data along the predfined 
    lat/lonl line 
    
    Parameters: 
    -----------
    
        ds : dataarray 
        lat_line: the latitude line you want to interpolate to 
        lon_line: the longitude line you want to interpolate to 
        
        
    Output: 
    --------
    
        line_data_array: numpy array with dimensions z, x
        
    '''
    # Prepare to store interpolated data
   
    line_data_list = []
 
    lon = ds.lon
    lat = ds.lat
    lon2d, lat2d = np.meshgrid(lon, lat)
     

    # Interpolate data along the line for each z level
    for z_level in ds['z']:
        data_at_z = ds.sel(z=z_level).values
        line_data = griddata(
            (lat2d.ravel(), lon2d.ravel()),  # Original grid points
            data_at_z.ravel(),                         # Original data values
            (lat_line, lon_line),                             # Interpolation points
            method='linear'                                   # Interpolation method
        )
        line_data_list.append(line_data)

    # Combine interpolated data into a new 2D array with dimensions (z, distance)
    line_data_array = np.stack(line_data_list) 

    return line_data_array

def calc_distance(lat_line, lon_line): 
    '''
    Calculates the distances between the points in lat_line and lon_line using 
    the haversine formula. 
    '''
    # Calculate cumulative distances along the line
    distances = [0]
    num_points = len(lat_line)
    for i in range(1, num_points):
        dist = haversine(lat_line[i-1], lon_line[i-1], lat_line[i], lon_line[i])
        distances.append(distances[-1] + dist)
    distances = np.array(distances)

    return distances
