import numpy as np
from modcor.modfun.fun_moisture import thermo_rh
from modcor.modfun.fun_heatflux import sfcflux_bulk
import initdata.init_fun as init
import modcor.constants as c

xmax      = 100            # How far you want to integrate (in km)
deltax    = 0.1             #Resolution (in km) (doesn't work well if you make it coarser)
x         = np.arange(deltax,xmax+deltax,deltax)*1e3

hepsilon = 0.002  # Convergence criterion 
N = 3000          # max  ABL height you expect through the iteration (how large you initialize arrays)


moisture  = True
ocean     = True
seaice    = False
tau       = 8*24*60*60      #"Length" of CAO outbreak on the ocean in seconds


# ABL parametrs
thetam0   = 253             # ABL mixed layer temperature
gammath0 = 0.012            # Background stability
um0     = 10                # mixed layer wind speed
h0      = 100               # Initial ABL height
qv0     = 0.30e-3           # Intial moisture content
qc0     = 0                 #No liquid water to start out with
upsilon = 0.2               #Entrainment parameter
qb0     = 0                 #background moistre profile; not IMPLEMENTED
pm0     = 1000

# Ice quantities
ice_init  = np.zeros(len(x))#init.eaMIZ_fun(int(90/deltax), x, 'tanhL')#
OW    = 1-ice_init#np.ones(len(x))
SI      = 'none'



#Ocean mixed layer quantities
mld0    = (np.ones(len(x))*23).astype(int)       #Initial mixed layer depth
depth_m = 500          # Depth of mixed layer profiles 

Tprofile0, Sprofile0 = init.init_CTD(75.5, -7.5, 30, ideal = True, x = x)

for i in range(len(x)): 
    Tprofile0[i,0:mld0[i]] = np.nanmean(Tprofile0[i,0:mld0[i]])#Tm0[0]
    Sprofile0[i,0:mld0[i]] = np.nanmean(Sprofile0[i,0:mld0[i]])#Sm0[0]

Tprofile = Tprofile0.copy()
Sprofile = Sprofile0.copy()
mld = mld0.copy()
Tm0 = Tprofile0[:,0]
Sm0 = Sprofile0[:,0]




# End of Part where you should change stuff! 
