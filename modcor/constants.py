
g  = 9.81           # gravity 
cp = 1004           # specific heat capacity of air 
R = 287             # gas constant

Ls = 2.836e6        # latent heat of sublimation
Lv  = 2.5e6         #latent heat of vaporization
eps = 0.622         #epsilon = Rd/Rv
es0 = 0.611e3       #reference saturation vapour pressure Pa
tk0 = 273.15        #reference temperature

Rv  = 461 #J/(Kkg)
k1  = 0.2e-3#0.2e-3#0.2e-5    #autoconversion rate, or precipitation efficiency (this is currently scaled up two order of magnitudes)
K = R/cp            #kappa


p0 = 1000 # Reference pressure 

#Ocean constants 
rhoI = 917
cpO = 4000
S_ice = 5
rhoO = 1027
Li = 33e4

# Transfer coefficient
Ch = 1.5e-3  # Transfer coefficient for sensible heat
Ce = 2.0e-3  # Transfer coeffieicnt for latent heat
