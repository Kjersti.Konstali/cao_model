import numpy as np
import modcor.constants as c

def sfcflux_bulk(sst, thetaL, qsw, qt, u): 
    '''
    Calculate heat fluxes based on bulk formulation

    Parameters:
    ------------
        sst    : sea surface temperatures
        thetaL : liquid water potential temperature
        qsw    :
        qt     : 
        u      : wind speed

    Returns
    ------------
        shf    : surface sensible heat flux
        lhf    : surface latent heat flux
    '''


    shf = c.Ch * u * (sst - thetaL)
    lhf = c.Ce * u * (qsw - qt)

    return shf, lhf
