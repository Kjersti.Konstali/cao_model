'''
This is the core of the model that calls other functions and does all the calculations. 
'''

import numpy as np
import xarray as xr

from modcor.modfun.fun_heatflux import sfcflux_bulk
import modcor.modfun.fun_thermo as fun_thermo
import modcor.constants as c
import modcor.modfun.fun_ocean as fun_ocean
import modcor.modfun.fun_moisture as fun_moisture
from modcor.modfun.progress_bar import update_progress



#%%

def modCor(mi):
    '''
    CAO model core. Calculates the changes in boundary layer height and ocean properties
    as a distance from the sea ice edge. 

    Parameters : 
    ------------
        mi : (module) "link" to file where the initialconditions are
    
    Returns: 
    ------------
        ds : (dataset) dataset containing (almost all) variables from the run. 
    
    '''
    #-------------------------------------------
    ## Do some initial calculations
    #-------------------------------------------
    a         = (mi.pm0/1000)**c.K     #Coefficient for potential temperature
    mi.tm0       = mi.thetam0*a         #Temperture at reference pressure given in K
    mi.T0        = mi.tm0-273.15        #Initial temperature given in C
    mi.rhom0     = (mi.pm0*100)/(c.R*mi.tm0) #Mixed layer density using surface pressure
    mi.P0        = 0                 #Initial precipitation
    #Initial values calculations
    __,__,qsat0,__,__ = fun_thermo.thermo_rh(mi.thetam0,mi.pm0,100)    #Saturation mixing ratio of the air

    #saturation mixing ratio close to the surface. 98% to account for salinity (Talley)
    __,__,qs0_sst,__,__= fun_thermo.thermo_rh(mi.Tm0[0],mi.pm0,98)
    mi.qm0  = mi.qv0 + mi.qc0                              #Total mixing ratio
    mi.thL0 = mi.thetam0 - c.Lv*mi.thetam0/(c.cp*mi.T0)*mi.qc0   #liquid water potential Temperature
    #Remember, that if you don't include moisture thL = thetam0

    #Calculate the fluxes with the bulk flux routine
    mi.QH0, mi.QE0  = sfcflux_bulk(mi.Tm0[0],mi.thL0,qs0_sst,mi.qm0,mi.um0)

    #Multiply sensible heat flux with the fraction of open water:
    mi.hsf0 = mi.QH0*mi.OW[0]

    #And same with latent heat flux:
    mi.lhf0 = mi.QE0*mi.OW[0]*mi.moisture

    #jump in temperature at the top of the mixed layer
    mi.dTH0 = mi.gammath0*mi.h0


    def make_ABL_profiles(i,zc, he,thL, ql, qp, qvprof, ABLql_moisture, ABLqp_moisture, ABLqv_moisture,ABL_profile): 
        '''
        Makes ABL profiles  (quite unelegantly)

        Parameters:
        -----------
            i      : (int) iteration
            zc     : (array) cloud base height
            he     : (array) ABL height
            thL    : (array) Liquid water potential temperature
            ql     : (array) Liquid water profile
            qp     : (array) Precpitable water profile
            qvprof : (array) vapour profile
            ABLqlmoisture (array) : array to stuff things in
            ABLqpmoisture (array) : --
            ABLqvmoisture (array) : --
            ABLprofile    (array) : --

        Returns: 
        ---------
            ABLprofile (array)    : updated ABL theta profile
            ABLqlmoisture (Array) : updated ABL liquid water profile
            ABLqpmoisture (array) : updated ABL precipitatable water profile
            ABLqvmpisture (array) : updated ABL water vapour profile

        
        '''
        envtheta = mi.thetam0 + np.arange(int(he[i]),mi.N)*mi.gammath0
        if mi.moisture:
            #create profiles for when moisture is included
            if zc[i]<he[i]:
                #if condensation occurrs 
                tprof = fun_moisture.Tprofilev3_fun(ql,thL[i]) #calculates the potential temperature profile within the boundary layer
                if np.size(tprof)==1:
                    tprof = np.array([tprof])
                ABLql_moisture[i,0:len(ql)] = ql    #Liquid water 
                ABLqp_moisture[i,0:len(qp)] = qp    #precipitable water. 
                ABLqv_moisture[i,0:len(qvprof)] = qvprof #vapour 
                _temp_temp  = np.concatenate((tprof,envtheta),axis=0)[0:mi.N]
                ABL_profile[i,0:len(_temp_temp)] = _temp_temp #the temperature profile of the boundary layer 
            else:
                _temp_temp = np.concatenate((np.ones(int(he[i]))*thL[i],envtheta),axis=0)[0:mi.N] #much easier if no condensation
                ABL_profile[i,0:len(_temp_temp)] = _temp_temp
                ABLqv_moisture[i,0:len(qvprof)] = qvprof #vapour 

        else:
            _temp_temp = np.concatenate((np.ones(int(he[i]))*thL[i],envtheta),axis=0)
    
        if len(_temp_temp)>2999: #just make sure that you don't try to stuff a large array into a smaller array 
            ABL_profile[i,0:mi.N] =  _temp_temp[0:mi.N] 
        else:
            ABL_profile[i,0:len(_temp_temp)] = _temp_temp 
    
        return ABL_profile, ABLql_moisture, ABLqp_moisture, ABLqv_moisture 
    
    
    def pros_ABL(i,dTH, thL, he, qm, dx, hsf, lhf, P):
        '''
        Calculates changes in the jump in potential temperature on the top of the ABL, 
        liquid water potential temperature, boundary layer height, and moisture due to entrainment
    
        Paramters: 
        ----------
            i   : (int) iteration number
            dTH : (array), jump in potential temperature
            thL : (array) liquid water potential temperature
            he  : (array) boundary layer height
            qm  : (array) mixing ratio (or specific humidity? )
            dx  : (array) grid spacing
            hsf : (array) sensible heat flux
            lhf : (array) latenth eat flux
            P   : (array) Precipitation
        
        
        Returns: 
        --------
            dTH : (array) jump in potential temperature (updated in index = i)
            thL : (array) updated thetal
            he  : (array) updated ABL height
            qm  : (array) updated mixed layer height
            
        ''' 
        # Jump in potential temperature on top of boundary layer
        if mi.upsilon>0: 
            #eq. 2.89
            dTH[i] = mi.dTH0 + mi.upsilon/mi.um0 * dx[0]/2 * (mi.gammath0*mi.hsf0/mi.dTH0 
                     + mi.gammath0*hsf[i]/dTH[i] + 2 * np.sum(mi.gammath0*hsf[0:i]/dTH[0:i]) -
                     thL[i] + mi.thL0) 
    
        else:
            # If there is no entrainment, there is no jump in potential temperature
            # But we set it to 1 now so that the functions below can be used (mi.upsilon = 0), 
            # (term is originally divided by dTH, so blows up if dTH  = 0)
            # unfortuntaly, you must update all values in the array 
            dTH[:] = 1

        if dTH[i]<0:
            print('fix not implemented yet! ABORT! ')
    
    
        # Change in LWPT is due to entrainment from above and the heat fluxes 
        thL[i] = (mi.thL0 + mi.upsilon/mi.um0 * dx[0]/2*(
            (mi.hsf0*mi.gammath0*mi.h0/(mi.dTH0*mi.h0)) +  
            hsf[i]*(mi.gammath0*he[i] + mi.thL0 - thL[i]) / (dTH[i]*he[i]) +
            2 * np.sum(hsf[0:i]*(mi.gammath0*he[0:i] + mi.thL0 - thL[0:i])/(dTH[0:i]*he[0:i])))
            + dx[0]/2 * 1/mi.um0 * (mi.hsf0 / mi.h0 + hsf[i]/he[i] + 
            2 * np.sum(hsf[0:i]/he[0:i]) + 
            (mi.P0/mi.h0 + P[i]/he[i] + 2 * np.sum(P[0:i]/he[0:i])*(c.Lv/c.cp))))
        
        #Moisture is lost to the free atmposphere 
        if mi.moisture: 
            qm[i] = (mi.qm0 + mi.upsilon/mi.um0 * dx[0]/2 * (mi.hsf0/(mi.dTH0*mi.h0) * (mi.qb0 - mi.qm0) + 
                    hsf[i]/(dTH[i]*he[i]) * (mi.qb0-qm[i]) + 2 * np.sum(hsf[0:i]/(dTH[0:i]*he[0:i]) * (mi.qb0 - qm[0:i])))
                    + dx[0]/2 * 1/mi.um0 * (mi.lhf0/mi.h0 + lhf[i]/he[i] + 2*np.sum(lhf[0:i]/he[0:i])) -
                    dx[0]/2 * 1/mi.um0 *  (mi.P0/mi.h0 +P[i]/he[i] + 2*np.sum(P[0:i]/he[0:i])))    
        
        # if there is no entrainment, we set it back to zero!
        if mi.upsilon==0: 
            dTH[:] = 0
        
        #Matching condition of jump
        he[i]   = 1/mi.gammath0 * (dTH[i] + thL[i] - mi.thL0)


        return dTH, thL, he, qm
    #%%
    
    a       = (mi.pm0/1000)**c.K     #Coefficient for potential temperature
    # First; must initialize stuff
    dx      = np.zeros(len(mi.x))    #dx = grid in x-direction
    he      = dx.copy()    #he - boundary layer height
    thL     = dx.copy()    #ML liquid water potential temperature
    hsf     = dx.copy()    #Sensible heat flux
    lhf     = dx.copy()    #Latent heat flux
    rhom    = dx.copy()    #density of the atmosphere
    tm      = dx.copy()    #potential temperature at reference pressure
    qm      = dx.copy()    #total moisture in the mixed layer
    P       = dx.copy()    #precipitation rate
    zc      = dx.copy()    #cloud base
    dTH     = dx.copy()    #jump in temperature at the top of ABL
    th_he   = dx.copy()    #temperature at the top of the ABL when qc>0
    ice     = dx.copy()
    mld     = mi.mld.copy() #Mixed layer depths
    Tm      = mi.Tm0.copy() #Mixed layer temperature
    Sm      = mi.Sm0.copy() #Mixed layer salinity 
    
    Tmprofile = np.ones((len(mi.x),mi.depth_m))
    Smprofile = Tmprofile.copy()
    
    #Vertical profiles 
    ABL_profile = np.zeros((len(mi.x),mi.N)) #3000 is just an arbitrary number
    ABLql_moisture = ABL_profile.copy() #Moisture (liquid)profiles
    ABLqp_moisture = ABL_profile.copy() #Moisture (precipitable wate) profiles
    ABLqv_moisture = ABL_profile.copy() #Moisture (vapour) profiles
    
    #Do some first guesses! 
    dTH[0] = mi.dTH0; he[0] = mi.h0; qm[0] = mi.qm0; thL[0] = mi.thL0; 
    dx[0]    = mi.x[0]-0
    
    #Do entrainment
    QH0, QE0  = sfcflux_bulk(mi.Tm0[0],mi.thL0,qs0_sst,mi.qm0,mi.um0)
    hsf[0] = QH0*mi.OW[0]
    lhf[0] = QE0*mi.OW[0]*mi.moisture
    dTH, thL, he, qm = pros_ABL(0,dTH, thL, he, qm, dx, hsf, lhf, P)


    # Need this for the solution to look nice in i = 0
    i = 0
    if mi.ocean: 
          QO = -(mi.hsf0*c.cp*mi.rhom0 + mi.hsf0*c.Lv*rhom[i])/(c.cpO*c.rhoO)
          Tm0 = mi.Tm0[i] - mi.tau * ( - QO)/mld[i]
          Sm0 = mi.Sm0[i] - mi.tau * ( - mi.Sm0[i]/(mi.lhf0-mi.P0)/c.rhoO)/mi.mld[i]
        
          Tprofile = np.concatenate((np.ones(int(mld[i]))*Tm0, mi.Tprofile[i,int(mld[i]):]),axis=0)
          Sprofile = np.concatenate((np.ones(int(mld[i]))*Sm0, mi.Sprofile[i,int(mld[i]):]),axis=0)

    else: 
         Tm = mi.Tm.copy()
         Sm = mi.Sm.copy()
   
    # Need to calculate the saturation mixing ratio again close to the sea surface
    __,__,qs0_sst,__,__= fun_thermo.thermo_rh(mi.Tm0[0],mi.pm0,98) #98 is to account for the effect of salinity 

    # Calculate new surface fluxes with the new SST, thL, qs0
    QH, QE  = sfcflux_bulk(mi.Tm0[0],thL[0],qs0_sst,qm[0],mi.um0)

    # tiling method for the heat fluxes if ice is present: 
    hsf[0] = QH*mi.OW[0]
    lhf[0] = QE*mi.OW[0]*mi.moisture
  
    oldh    = 0 
    itera   = 0 #counter for how many iterations you have to keep the program for running to infinity
    tm[0]     = thL[0]*(mi.pm0/1000)**c.K     #Coefficient for potential temperature
    rhom[0]   = (mi.pm0*100)/(c.R*tm[0]) #density of the air

    #%%
    # Main loop: 
    for i in range(0,len(mi.x)): 
        update_progress(i/len(mi.x))
        if i > 0: 
            itera = 0
            lhf[i]          = lhf[i-1]
            hsf[i]          = hsf[i-1]
            dx[i]           = mi.x[i] - mi.x[i-1]
            th_he[i]        = th_he[i-1]
            he[i]           = he[i-1]
            qm[i]           = qm[i-1]
            thL[i]          = thL[i-1]
            dTH[i]          = dTH[i-1]
            rhom[i]         = rhom[i-1]
            mld[i]          = mi.mld[i]
            oldh = 0

        while abs(he[i]-oldh) > mi.hepsilon or itera < 3: 
            oldh = he[i]
            
            dTH, thL, he, qm = pros_ABL(i,dTH, thL, he, qm, dx, hsf, lhf, P)
            zc[i] = fun_moisture.cloud_fun(thL[i], qm[i], rhom[i])
            if not mi.moisture: 
                zc[i] = 9999
            P[i],qp,ql,theta_prof,qvprof= fun_moisture.precipRK4_fun(zc[i],thL[i],he[i],qm[i])
            

            #precipitation calculated based on the thickness of the cloud; 
            # and also separates the vapour and liquid apart

        
            tm[i]     = np.mean(theta_prof)*a
            rhom[i]   = (mi.pm0*100)/(c.R*tm[i])

            if mi.ocean: 
                #This shouldn't be here, but it dind't work within the function :S 
                QO = -(hsf[i]*c.cp*rhom[i] + lhf[i]*c.Lv*rhom[i])/(c.cpO*c.rhoO)
                Tm[i] = mi.Tm0[i] - mi.tau * ( - QO)/mld[i]
                Sm[i] = mi.Sm0[i] - mi.tau * ( - Sm[i]*(lhf[i]-P[i])/c.rhoO)/mld[i]
                
                Tprofile = np.concatenate((np.ones(int(mld[i]))*Tm[i], mi.Tprofile[i,int(mld[i]):]), axis=0)
                Sprofile = np.concatenate((np.ones(int(mld[i]))*Sm[i], mi.Sprofile[i,int(mld[i]):]), axis=0)
                Tprofile,Sprofile,mld[i] = fun_ocean.profile_convection(mld[i],Tprofile,Sprofile)

                
                if mi.seaice: 
                    
                    new_mld, Tprofile, Sprofile, I = fun_ocean.ice_fun(i, mld,Tprofile,Sprofile,ice)
                    mld[i] = new_mld
                    ice[i] = I
                    
                
                Tm[i] = Tprofile[0]
                Sm[i] = Sprofile[0]

            else:
                Tm[i] = mi.Tprofile[i,0]
                Sm[i] = mi.Sprofile[i,0]
            
            __,__,qs0_sst,__,__= fun_thermo.thermo_rh(Tm[i],mi.pm0,98)
            __,__,qsat,__,__   = fun_thermo.thermo_rh(theta_prof[0],mi.pm0,100)

            QH, QE  = sfcflux_bulk(Tm[i],theta_prof[0],qs0_sst,qm[i],mi.um0)

            hsf[i]  = QH*mi.OW[i]
            lhf[i]  = QE*mi.OW[i]*mi.moisture

    
            itera = itera + 1

        #Make some profiles
        ABL_profile, ABLql_moisture, ABLqp_moisture, ABLqv_moisture = make_ABL_profiles(i,zc, he,thL, ql, qp, qvprof, 
                                                                                        ABLql_moisture, ABLqp_moisture, ABLqv_moisture,ABL_profile)

        Tmprofile[i,0:int(mld[i])] = Tm[i]
        Tmprofile[i,int(mld[i]):mi.depth_m] = mi.Tprofile[i,int(mld[i]):mi.depth_m]
        Smprofile[i,0:int(mld[i])] = Sm[i]
        Smprofile[i,int(mld[i]):mi.depth_m] = mi.Sprofile[i, int(mld[i]):mi.depth_m]
        
    
    ds = xr.Dataset(data_vars=dict(
        he  = (['x'], he),
        thL = (['x'], thL),
        qm  = (['x'], qm), 
        lhf = (['x'], lhf*rhom*c.Lv),
        hsf = (['x'], hsf*rhom*c.cp),
        P   = (['x'], P*24*60*60),
        zc  = (['x'], zc),
        Tm  = (['x'], Tm),
        Sm  = (['x'], Sm),
        ice = (['x'], ice),
        rho = (['x'], rhom), 
        dTH = (['x'], dTH),
        
        theta_profile = (['z','x'], ABL_profile.T),
        ql_profile    = (['z','x'], ABLql_moisture.T),
        qp_profile    = (['z','x'], ABLqp_moisture.T),
        qv_profile    = (['z','x'], ABLqv_moisture.T), 
        
        mld = (['x'], mld),        
        T_profile  = (['depth','x'], Tmprofile[:,0:mi.depth_m].T),
        S_profile   = (['depth','x'], Smprofile[:,0:mi.depth_m].T,),
        ),
        coords = dict(
            x = (['x'], mi.x/1e3),
            z = (['z'], np.arange(mi.N)),
            depth = (['depth'], np.arange(mi.depth_m))
        ),
        attrs = {'um0':mi.um0, 'qm0':mi.qm0, 'moisture': int(mi.moisture),
                 'tau':mi.tau, 'tm0':mi.Tm0, 'sm0':mi.Sm0, 
                 'thL0':mi.thetam0, 'gammath':mi.gammath0,
                 'ups':mi.upsilon, 'SI':mi.SI, 'mld0':mi.mld,
                 'dx':mi.x[1]-mi.x[0], 'seaice': int(mi.seaice),
                 'ocean': int(mi.ocean), 'h0':mi.h0, 'OW':mi.OW})

    return ds