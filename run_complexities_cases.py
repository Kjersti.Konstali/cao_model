
#%%
from modcor.main import modCor

#%%
# Instead of using these classes, you can just import the initial values directly
#and pass them onto the modCor function... not sure about the best practice here.   
class reference(object):
    name = 'reference'
    def __init__(self, name):
        self.name = name
    import initdata.modinit_reference_long as mi
    
class test(object): 
    name = 'ocean_prof_test_NABOS_SI_T8_444'
    def __init__(self, name):
        self.name = name
    import initdata.modinit_ocean_prof_test as mi
    
#%%

for t in [reference]:
    mi = t.mi
    for moist in [False, True]: 
        mi.moisture = moist
        for tau in [0, 1, 2, 5]: 
            mi.tau = tau*24*60*60
            if tau > 0 :
                mi.ocean = True
            else: 
                mi.ocean = False
            for ups in [0, 0.1, 0.2]: 
                    mi.upsilon=ups
            
                    t.name = 'complex_{}_moist{}_ups{}'.format(tau,moist,ups*10)
                    print(t.name)
                  
                    ds = modCor(mi)
                    comp = dict(zlib=True, complevel=9)
                    encoding = {var: comp for var in ds.data_vars}    
                
                    ds.to_netcdf('/scratch/kjersti/cao_model_v2/runs/complexities/{}.nc'.format(t.name), encoding=encoding)
                
