#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  9 09:17:54 2024

@author: kjersti
"""


#%%
from modcor.main import modCor
import initdata.init_fun as init


#%%
# INstead of using these classes, you can just import the initial values directly
#and pass them onto the modCor function... not sure about the best practice here.   
class reference(object):
    name = 'reference'
    def __init__(self, name):
        self.name = name
    import initdata.modinit_reference as mi
    
#%%

for t in [reference]:
    mi = t.mi
    for moist in [False, True]: 
        mi.moisture = moist
        for tau in [0, 1, 2, 5]: 
            mi.tau = tau*24*60*60
            if tau > 0 :
                mi.ocean = True
            else: 
                mi.ocean = False
            for L in [45, 90, 180]: 
                for SI in ['step','lin','tanhU','tanh','tanhL']: 
                    mi.seaice = True
                    mi.ice_init   = init.eaMIZ_fun(int(L/mi.deltax), mi.x, SI)#
                    mi.OW    = 1-mi.ice_init
                    mi.SI    = SI
            
                    t.name = 'SI_tau{}_moist{}_L{}_SI{}'.format(tau,moist,L,SI)
                    print(t.name)
                  
                    ds = modCor(mi)
                    comp = dict(zlib=True, complevel=9)
                    encoding = {var: comp for var in ds.data_vars}    
                
                    ds.to_netcdf(f'./runs/SI/{t.name}.nc', encoding=encoding)
                
