import proplot as pplt


import base_plot as bp
import xarray as xr
import glob
import calc_budgets as cb
import re
import os


#%%
to_comp = '../runs/SI/SI_tau*_moist*tanh.nc'
search_dir ='../runs/SI/'

files = glob.glob(to_comp)
files.sort()
comp_dict = {}

for file in files: 
    print(file)
    comp_dict[file.split('/')[-1][0:-3]] = xr.open_dataset(file)

kkeys = [k for k in comp_dict]



print(kkeys)

#%%
fig,axs = bp.base_plot(comp_dict)



#%%

fig2, axs2 = bp.ABLprofiles_plot(comp_dict[kkeys[0]])

#%%

fig3, axs3 = bp.OMLprofiles_plot(comp_dict[kkeys[-1]])



#%% 

def find_files(search_dir, expression1, expression2='',expression3='',expression4=''):
    pattern = re.compile(rf"(?=.*{expression1})(?=.*{expression2})(?=.*{expression3})(?=.*{expression4})")
    matching_files = []
    for root, dirs, files in os.walk(search_dir):
        for file in files:
            if pattern.search(file):
                matching_files.append(os.path.join(root, file))
    return matching_files


#%% Complexities plots: 
import pandas as pd

taus = list(set([f.split('tau')[-1][0] for f in files]))
taus.sort()

ups = list(set([f.split('L')[-1].split('_')[0] for f in glob.glob(to_comp)]))
ups.sort()

moists = list(set([f.split('moist')[-1][0] for f in glob.glob(to_comp)]))
hsf = {}
lhf = {}
budget_dict = {}
for moisture in moists: 
    lhf[moisture] = pd.DataFrame(index = taus, columns = ups, dtype =float)
    hsf[moisture] = pd.DataFrame(index = taus, columns = ups, dtype =float)
    budget_dict[moisture] = {}
    for tau in taus:
        budget_dict[moisture][tau] = {}
   
        for up in ups: 


            file = find_files(search_dir, f'tau{tau}', moisture, up,'tanh.nc')
            print(file)
            
            ds = xr.open_dataset(file[0])
            _hsf, _lhf = cb.int_heatflux(ds)
            hsf[moisture].loc[tau,up] = _hsf
            lhf[moisture].loc[tau,up] = _lhf      

            budget_dict[moisture][tau][up] = cb.totalBudgets(ds,['heat'])
            



#%%
fig,axs = pplt.subplots(ncols = 2, nrows=2, width = 5)
for m,ax in zip(moists,axs[0,:]): 
    cb1 = ax.heatmap(hsf[m]/1e6, labels = True, precision=0, extend = 'both', cmap = 'viridis', vmin = 6e3, vmax = 9e3)
for m,ax in zip(moists,axs[1,:]): 
    cb2 = ax.heatmap(lhf[m]/1e9, shading = 'flat', labels = True, precision=0, cmap = 'viridis' , extend = 'both', vmin = 1e4, vmax = 1.4e4)    
axs.format(collabels=moists, ylabel = r'$\tau$ [days]', rowlabels = ['sensible heat flux', 
                                                                     'latent heat flux'], 
           xrotation = 45)
axs[0,-1].colorbar(cb1, loc = 'r')
axs[1,-1].colorbar(cb2, loc = 'r')


#%% Calculating budgets
fig,ax = pplt.subplots(ncols = 2)


ax[0].bar(budget_dict['T']['5']['45']['heat'])    
ax[1].bar(budget_dict['T']['0']['45']['heat'])
ax[1].bar(budget_dict['T']['0']['180']['heat'], alpha = 0.5)
ax[1].bar(budget_dict['T']['0']['90']['heat'], alpha = 0.5)

ax.format(xrotation=45)
#%%



fig,ax = pplt.subplots()
ax.bar(budget_test['heat'])

