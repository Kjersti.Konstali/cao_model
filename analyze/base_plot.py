import proplot as pplt
import numpy as np



def __plot_all(axs, ds, C, lbl): 
    axs[0].plot(ds.he, label = lbl, legend = 'lr')
    axs[0].plot(ds.where(ds.he>ds.zc).zc, color = C, ls = '--')
    axs[0].fill_between(ds.x, ds.where(ds.he>ds.zc).zc, ds.he, color = C, alpha = 0.3)
    axs[0].format(title='ABL height and Cloud Base height', ylabel = '[m]')

    axs[2].plot(ds.hsf, c = C)
    axs[2].plot(ds.lhf, c = C, ls = '--')
    axs[2].format(title='Sensible and Latent Heat Fluxes', ylabel = r'$[W/m^2]$')

    axs[4].plot(ds.qm*1000, c = C)
    axs[4].format(title='Total mixing ratio', ylabel = '[g/kg]' )

    axs[6].plot(ds.P, c = C)
    axs[6].format(title='Precipitation', ylabel = '[mm/day]')

    axs[1].plot(ds.Tm, c = C)
    #axs[1].plot(ds.x, t.mi.Tm, color = 'gray', linewidth = 2, alpha = 0.6)
    axs[1].format(title='OML Temperature', ylabel = '[K]')

    axs[3].plot(ds.Sm, c = C)
    axs[3].format(title='OML Salinity', ylabel = '[psu]')

    axs[5].plot(ds.mld, c = C)

    axs[5].format(yreverse=True, title='Mixed layer depth', ylabel = '[m]')

    axs[7].plot(ds.ice, c = C)
    axs[7].format(title='Sea ice formation', ylabel = '[m]')
    #axb = axs[7].twinx()
    #axb.plot(ds.x.values,1-t.mi.OW, color = 'gray', alpha  =.4)
    #axb.format(ylabel='Ice fraction')
    axs.format(xlabel = 'Distance from sea ice edge [km]')

    return axs

def base_plot(ds_comp): 
    '''
    Plot basic parameters as function of distance from sea ice edge; 
    he, zc, thL, hsf, lhf, mixing ratio, precipitation, OML temperature, 
    OML salinity, mld, sea ice formation. 
    
    Parameters: 
    -----------
        ds: (dataset or dictionary) modeloutput(s), if dictionary with datasets,
            it plots all datasets with the keys as labels
    
    Returns: 
    -----------
        fig : figure handle
        axs : axs
    '''
    fig,axs = pplt.subplots(nrows = 4, ncols = 2, refaspect = 2.5, sharey=False, width = 10)

    if type(ds_comp)==dict: 
        for k,ds_c in enumerate(ds_comp): 
            ds = ds_comp[ds_c]
            C  = 'C{}'.format(k)
            axs = __plot_all(axs, ds, C, ds_c)

    else: 
        axs = __plot_all(axs, ds_comp, 'C0', lbl=None)


    return fig, axs



def ABLprofiles_plot(ds):
    '''
    Plot ABL profiles of potential temperature and liquid water 
    
    Parameters: 
    -----------
        ds: (dataset) modeloutput
    
    Returns: 
    -----------
        fig : figure handle
        axs : axs
    '''
    fig,axs = pplt.subplots(nrows = 2, refaspect = 2.5, sharey=False, width = 6)

    axs[0].pcolormesh(ds.theta_profile.where(ds.z<ds.he), colorbar = 'b', levels = pplt.arange(252,272),
    extend='both', cmap = 'Magma', colorbar_kw = {'label':r'$Potential\ temperature\  (\theta)$'}, 
    )
    axs[0].format()
    axs[1].pcolormesh(ds.ql_profile.where((ds.z<ds.he) & (ds.ql_profile>0))*1000, colorbar='b', levels = np.linspace(0,0.4,10),
    cmap = 'YlGnBu', colorbar_kw = {'label' : 'Liquid water profile'})

    for ax in axs: 
        ax.plot(ds.zc, color = 'k', lw = 0.8, ls = '--')
        ax.plot(ds.he, color = 'k', lw = 0.8)

    axs.format(ylim=[0,ds.he.max()+100], ylabel = 'ABL height [m]', xlabel = 'Distance from sea ice edge [km]')

    return fig,axs 


def OMLprofiles_plot(ds):
    '''
    Plot OML profiles of temperature and salinity
    
    Parameters: 
    -----------
        ds: (dataset) modeloutput
    
    Returns: 
    -----------
        fig : figure handle
        axs : axs

    '''
    fig,axs = pplt.subplots(nrows = 2, refaspect = 2.5, sharey=False, width = 6)

    axs[0].pcolormesh(ds.T_profile-273.15, colorbar = 'r', levels = pplt.arange(-1.8,4, 0.2),
    extend='both', cmap = 'BuRd', norm = pplt.Norm('diverging', fair=False))

    axs[1].pcolormesh(ds.S_profile, colorbar='r',
    cmap = 'haline', levels = pplt.arange(30,36,0.5))

    for ax in axs: 
        ax.plot(ds.mld, color = 'k')

    axs.format(ylim=[150,0], ylabel = 'OML depth [m]', xlabel = 'Distance from sea ice edge [km]')



    return fig,axs 



    
    
